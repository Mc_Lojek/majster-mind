package com.example.patryk.mastermind;

/**
 * Created by Patryk on 2018-04-21.
 */

public class Circle
{
    private boolean ifClear;
    private int color;
    private int positionX;
    private int positionY;

    public Circle(int color1,int positionX1,int positionY1)
    {
        color=color1;
        positionX=positionX1;
        positionY=positionY1;
    }

    public void setColor(int color)
    {
        if(color!=0)
        {
            ifClear=false;
        }
        else
        {
            ifClear=true;
        }
        this.color=color;
    }

    public void setPositionX(int positionX)
    {
        this.positionX=positionX;
    }

    public void setPositionY(int positionY)
    {
        this.positionY=positionY;
    }

    public int getColor()
    {
        return this.color;
    }

    public int getPositionX()
    {
        return this.positionX;
    }

    public int getPositionY()
    {
        return this.positionY;
    }

}
