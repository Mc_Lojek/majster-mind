package com.example.patryk.mastermind;

/**
 * Created by Patryk on 2018-04-21.
 */

import java.util.Random;

public class GameController
{
    private Circle[][] circles = new Circle[4][8];
    private Circle[] password = new Circle[4];
    private int perfect;
    private int good;

    public int getPerfect()
    {
        return this.perfect;
    }
    public int getGood()
    {
        return this.good;
    }

    public void setGood(int good)
    {
        this.good = good;
    }
    public void setPerfect(int perfect)
    {
        this.perfect = perfect;
    }

    public void initialize(int sizeX,int sizeY)
    {
        for(int i=0;i<sizeY;i++)//to jest y
        {
            for(int j=0;j<sizeX;j++)//to jest x
            {
                circles[j][i] = new Circle(0,j,i);
            }
        }
        perfect = 0;
        good = 0;
    }

    public void changeColor(int x, int y, int color)
    {
        circles[x][y].setColor(color);
    }

    public void generatePassword()
    {
        Random r = new Random();
        password[0] = new Circle(r.nextInt(6)+1,1,8);
        password[1] = new Circle(r.nextInt(6)+1,2,8);
        password[2] = new Circle(r.nextInt(6)+1,3,8);
        password[3] = new Circle(r.nextInt(6)+1,4,8);
    }

    public void turn(int y)
    {
        int curr_color[] = new int[4];
        int copy_color[] = new int[4];
        int pass_color[] = new int[4];
        int pass_color_copy[] = new int[4];
        curr_color[0] = circles[0][y].getColor();
        curr_color[1] = circles[1][y].getColor();
        curr_color[2] = circles[2][y].getColor();
        curr_color[3] = circles[3][y].getColor();

        copy_color[0] = circles[0][y].getColor();
        copy_color[1] = circles[1][y].getColor();
        copy_color[2] = circles[2][y].getColor();
        copy_color[3] = circles[3][y].getColor();

        pass_color[0] = password[0].getColor();
        pass_color[1] = password[1].getColor();
        pass_color[2] = password[2].getColor();
        pass_color[3] = password[3].getColor();

        pass_color_copy[0] = password[0].getColor();
        pass_color_copy[1] = password[1].getColor();
        pass_color_copy[2] = password[2].getColor();
        pass_color_copy[3] = password[3].getColor();

        for(int i = 0; i < 4; i++)
        {
            if(copy_color[i] == pass_color_copy[i])
            {
                perfect++;
                pass_color_copy[i] = 0;
                copy_color[i] = -1;
            }
        }
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                if(copy_color[j] == pass_color_copy[i])
                {
                    pass_color_copy[i] = 0;
                    copy_color[j] = -1;
                    good++;
                }
            }
        }
    }

    public String passCreate()
    {
        String result= "";
        result += password[0].getColor();
        result += password[1].getColor();
        result += password[2].getColor();
        result += password[3].getColor();

        return result;

    }
}