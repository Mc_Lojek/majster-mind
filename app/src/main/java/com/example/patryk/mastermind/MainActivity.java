package com.example.patryk.mastermind;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    private int C;
    private int HighestC;
    private boolean OkClicked;
    private boolean IfWin;
    private boolean LevelChanged;
    private GameController gc = new GameController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        C=0;
        gc.initialize(4,8);
        gc.generatePassword();
        OkClicked = false;
        IfWin = false;
        LevelChanged = false;
        HighestC = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_reset:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickRed(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin)
        {
            int x = C % 4;
            int y = C / 4;
            changeImage(x,y,1);
            gc.changeColor(x,y,1);
            afterClick();
        }

    }
    public void onClickBlue(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin)
        {
            int x = C % 4;
            int y = C / 4;
            changeImage(x,y,2);
            gc.changeColor(x,y,2);
            afterClick();
        }

    }

    public void onClickGreen(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin) {
            int x = C % 4;
            int y = C / 4;
            changeImage(x, y, 3);
            gc.changeColor(x, y, 3);
            afterClick();
        }
    }

    public void onClickOrange(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin) {
            int x = C % 4;
            int y = C / 4;
            changeImage(x, y, 4);
            gc.changeColor(x, y, 4);
            afterClick();
        }
    }

    public void onClickPink(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin) {
            int x = C % 4;
            int y = C / 4;
            changeImage(x, y, 5);
            gc.changeColor(x, y, 5);
            afterClick();
        }
    }

    public void onClickBlueLagoon(View view)
    {
        if((C%4!=0 || OkClicked || C==0) && !IfWin)
        {
            int x = C % 4;
            int y = C / 4;
            changeImage(x, y, 6);
            gc.changeColor(x, y, 6);
            afterClick();

        }
    }

    public void afterClick()
    {
        C++;
        LevelChanged = false;
        OkClicked = false;
    }

    public void changeImage(int x, int y, int color)
    {
            ImageView currentImage = findViewById(R.id.imageView00);
            int red = R.drawable.circle_red;
            int blue = R.drawable.circle_blue;
            int green = R.drawable.circle_green;
            int orange = R.drawable.circle_orange;
            int pink = R.drawable.circle_pink;
            int bluelagoon = R.drawable.circle_blue_lagune;
            int white = R.drawable.circle_white;

            switch(y)
            {
                case 0:
                {
                    switch(x)
                    {
                        case 0:
                    {
                        currentImage = (ImageView) findViewById(R.id.imageView00);
                    } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView10);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView20);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView30);
                        } break;
                    }
                } break;
                case 1:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView01);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView11);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView21);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView31);
                        } break;
                    }
                } break;
                case 2:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView02);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView12);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView22);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView32);
                        } break;
                    }
                } break;
                case 3:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView03);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView13);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView23);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView33);
                        } break;
                    }
                } break;
                case 4:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView04);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView14);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView24);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView34);
                        } break;
                    }
                } break;
                case 5:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView05);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView15);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView25);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView35);
                        } break;
                    }
                } break;
                case 6:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView06);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView16);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView26);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView36);
                        } break;
                    }
                } break;
                case 7:
                {
                    switch(x)
                    {
                        case 0:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView07);
                        } break;
                        case 1:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView17);
                        } break;
                        case 2:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView27);
                        } break;
                        case 3:
                        {
                            currentImage = (ImageView) findViewById(R.id.imageView37);
                        } break;
                    }
                } break;
            }

            switch(color)
            {
                case 0:
                {
                    currentImage.setImageResource(white);
                } break;
                case 1:
                {
                    currentImage.setImageResource(red);
                } break;
                case 2:
                {
                    currentImage.setImageResource(blue);
                } break;
                case 3:
                {
                    currentImage.setImageResource(green);
                } break;
                case 4:
                {
                    currentImage.setImageResource(orange);
                } break;
                case 5:
                {
                    currentImage.setImageResource(pink);
                } break;
                case 6:
                {
                    currentImage.setImageResource(bluelagoon);
                } break;
            }
    }

    public void scoreDisplay(int y)
    {
        TextView textViewG = (TextView) findViewById(R.id.textView0a);
        TextView textViewP = (TextView) findViewById(R.id.textView0b);

        switch(y)
        {
            case 0:
            {
                textViewG = (TextView) findViewById(R.id.textView0a);
                textViewP = (TextView) findViewById(R.id.textView0b);
            } break;
            case 1:
            {
                textViewG = (TextView) findViewById(R.id.textView1a);
                textViewP = (TextView) findViewById(R.id.textView1b);
            } break;
            case 2:
            {
                textViewG = (TextView) findViewById(R.id.textView2a);
                textViewP = (TextView) findViewById(R.id.textView2b);
            } break;
            case 3:
            {
                textViewG = (TextView) findViewById(R.id.textView3a);
                textViewP = (TextView) findViewById(R.id.textView3b);
            } break;
            case 4:
            {
                textViewG = (TextView) findViewById(R.id.textView4a);
                textViewP = (TextView) findViewById(R.id.textView4b);
            } break;
            case 5:
            {
                textViewG = (TextView) findViewById(R.id.textView5a);
                textViewP = (TextView) findViewById(R.id.textView5b);
            } break;
            case 6:
            {
                textViewG = (TextView) findViewById(R.id.textView6a);
                textViewP = (TextView) findViewById(R.id.textView6b);
            } break;
        }
        textViewG.setText(String.valueOf(gc.getGood()));
        textViewP.setText(String.valueOf(gc.getPerfect()));
    }

    public void onClickOk(View view)
    {
        if(C % 4 == 0 && C!=0)
        {
            int y = C / 4;
            gc.turn(y-1);
            scoreDisplay(y-1);
            if(gc.getPerfect()==4)
            {
                win();
            }
            gc.setPerfect(0);
            gc.setGood(0);
            OkClicked = true;
        }

        if(C == 32)
        {
            lose();
        }

    }

    public void onClickUndo(View view)
    {


        if(!OkClicked && C>0)
        {
            C--;
            int x = C % 4;
            int y = C / 4;

            changeImage(x,y,0);
            gc.changeColor(x,y,0);

            if(x == 0)
            {
                OkClicked = true;
            }
        }
    }

    public void win()
    {
        Context context = getApplicationContext();
        CharSequence text = "WYGRALES";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        IfWin = true;
    }

    public void lose()
    {
        String pass = gc.passCreate();
        Context context = getApplicationContext();
        CharSequence text = "PRZEGRALES " + pass;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        IfWin = true;
    }
}
